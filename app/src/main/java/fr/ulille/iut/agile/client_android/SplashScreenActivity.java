package fr.ulille.iut.agile.client_android;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {
    private final int SPLASH_SCREEN_TIMEOUT = 3000;
    private ProgressBar loadingBar;
    private final String url = "https://groupe8.azae.eu/api/v1/customers";
    private final String locations_url = "https://groupe8.azae.eu/api/v1/locations";
    private RequestQueue queue;
    private String storedId;
    private JSONObject user_profile;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        loadingBar = findViewById(R.id.loadingBar);
        loadingBar.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this, R.color.beige_parchemin), PorterDuff.Mode.SRC_IN );

        queue = Volley.newRequestQueue(getApplicationContext());

        storedId = PreferenceManager.
                getDefaultSharedPreferences(getApplicationContext()).getString("userID","");

        Log.d("user id", storedId);

        if(user_profile == null) user_profile = new JSONObject();

        ConnectivityManager manager = (ConnectivityManager)getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo i = manager.getActiveNetworkInfo();
        boolean hasConnect = (i!= null && i.isConnected() && i.isAvailable());

        if(hasConnect) {
            Log.d("user id", storedId);

            if (storedId != null && storedId!="") {
                String user_url = url + "/" + storedId;
                Log.d("user id", storedId);

                //GET request
                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, user_url, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // display response
                                Log.d("Response", response.toString());

                                String userDate = null;
                                String userFirstName = null;
                                String userLastName = null;
                                String userLocation = null;
                                String userMail = null;
                                String userTelNum = null;

                                try {
                                    userDate = response.getString("date");
                                    userFirstName = response.getString("firstName");
                                    userLastName = response.getString("lastName");
                                    userLocation = response.getString("location");
                                    userMail = response.getString("mail");
                                    userTelNum = response.getString("telNum");

                                    Log.d("firstname", userFirstName);

                                    user_profile.put("user_firstname", userFirstName);
                                    user_profile.put("user_lastname", userLastName);
                                    user_profile.put("user_date", userDate);
                                    user_profile.put("user_location", userLocation);
                                    user_profile.put("user_mail", userMail);
                                    user_profile.put("user_telnum", userTelNum);

                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                                            .putString("user_profile", user_profile.toString()).apply();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error.Response", error.toString());
                                storedId = "";
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                                        .putString("userID", storedId.toString()).apply();
                            }
                        }
                );
                queue.add(getRequest);
            }

            if(storedId!="" && storedId!=null) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, SPLASH_SCREEN_TIMEOUT);
            }else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), WelcomeSlider.class);
                        //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, SPLASH_SCREEN_TIMEOUT);
            }

        } else {
            showAlertDialog(SplashScreenActivity.this, "Erreur lors du chargement",
                    "Aucune connexion Internet détectée.\nVérifiez l'état de votre connexion et réessayer.", false);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        //Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.mipmap.ic_launcher : R.mipmap.ic_launcher);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                finish();
                System.exit(0);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
