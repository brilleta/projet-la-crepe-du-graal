package fr.ulille.iut.agile.client_android;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.IDNA;
import android.os.Build;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import interfaces.DialogCallback;
import utils.GlobalUtils;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private Fragment fragments[] = new Fragment[4];
    private Fragment selectedFragment=null;
    private final int REQUEST_CODE = 101;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation);

        this.configureBottomView();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            //w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.setStatusBarColor(Color.TRANSPARENT);
        }

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        boolean flag = false;
        if (extras != null){
            flag = extras.getBoolean("Flag");
        }

        if(flag) {
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_main_frame_layout, new InformationsTab()).commit();
        }else {
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_main_frame_layout, new CommandTab()).commit();
            bottomNavigationView.setSelectedItemId(R.id.action_command);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);
        }
    }

    //Configure BottomNavigationView Listener
    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> updateMainDisplay(item.getItemId()));
    }

    //Update Main application display
    private Boolean updateMainDisplay(Integer integer){
        selectedFragment = null;
        switch (integer) {
            case R.id.action_command:
                if (fragments[0]==null){
                    fragments[0]=new CommandTab();
                }
                selectedFragment = fragments[0];
                break;
            case R.id.action_informations:
                if (fragments[1]==null){
                    fragments[1] = new InformationsTab();
                }
                selectedFragment = fragments[1];
                break;
            case R.id.action_localisation:
                if (fragments[2]==null){
                    fragments[2] = new LocalisationTab();
                }
                selectedFragment = fragments[2];
                break;
            case R.id.action_contact:
                if (fragments[3]==null){
                    fragments[3] = new ContactTab();
                }
                selectedFragment = fragments[3];
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_main_frame_layout, selectedFragment, "CurrentFragment").commit();
        return true;
    }

    /*public void doTest(View view) {
        final TextView tvResultat = findViewById(R.id.resultat);
        final String server_url = getResources().getString(R.string.server_url);
        StringRequest request = new StringRequest(Request.Method.GET, server_url,
                (response) -> tvResultat.setText(response),
                (error) -> tvResultat.setText(error.toString()));
        request.setTag("VOLLEY");
        queue.add(request);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
