package fr.ulille.iut.agile.client_android;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class InformationsTab extends Fragment {

    View view;
    Spinner localisations;
    TextView mDisplayData;
    ImageButton buttonBirthday;
    TextView firstname;
    TextView lastname;
    TextView telNum;
    TextView mail;
    DatePickerDialog.OnDateSetListener mOnDateSetListener;
    private TextView location_display;
    private Button sendForm;
    private Button modiForm;
    private Button validModif;
    private final String url = "https://groupe8.azae.eu/api/v1/customers";
    private final String locations_url = "https://groupe8.azae.eu/api/v1/locations";
    private RequestQueue queue;
    private String storedId;
    private JSONObject user_profile;
    private List<String> spinnerPlaces;

    public InformationsTab() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.informations_tab, container, false);
        setRetainInstance(true);

        //Initialisation des widgets
        localisations = view.findViewById(R.id.location);
        buttonBirthday = (ImageButton) view.findViewById(R.id.birthday_button);
        mDisplayData = (TextView)  view.findViewById(R.id.date);
        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.lastname);
        mail = view.findViewById(R.id.mail);
        telNum = view.findViewById(R.id.telNum);
        sendForm = view.findViewById(R.id.sendFormButton);
        validModif = view.findViewById(R.id.validModif);
        modiForm = view.findViewById(R.id.modiForm);
        location_display = view.findViewById(R.id.location_display);
        location_display.setFocusable(false);

        //Initialisation de la file pour les requêtes serveur
        queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        //Initialisation des variables avec valeurs persistantes
        storedId = PreferenceManager.
        getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("userID","");
        String my_profile = PreferenceManager.
                getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("user_profile", "");

        Log.d("User profile infos", my_profile);

        //GET all locations request
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, locations_url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        spinnerPlaces = new ArrayList<>();
                        int spinnerDefault = 0;
                        for(int i=0; i<response.length(); i++) {
                            String place = null;
                            try {
                                place = response.getJSONObject(i).getString("places");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            spinnerPlaces.add(place);
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerPlaces);
                        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        localisations.setAdapter(adapter);
                        localisations.setSelection(spinnerDefault);
                    }
                },
                new Response.ErrorListener()
                {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );
        queue.add(getRequest);


        if(storedId!=null && storedId!="") {

            sendForm.setVisibility(View.INVISIBLE);
            modiForm.setVisibility(View.VISIBLE);

            setWidgetOff();

            if (my_profile != null && my_profile!="") {
                try {
                    JSONObject profile_informations = new JSONObject(my_profile);
                    getProfileInformations(profile_informations);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                modiForm.setVisibility(View.INVISIBLE);
                sendForm.setVisibility(View.VISIBLE);

                setWidgetsOn();
            }
        }else {
            modiForm.setVisibility(View.INVISIBLE);
            sendForm.setVisibility(View.VISIBLE);

            setWidgetsOn();
        }

        buttonBirthday.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog;
                if(!mDisplayData.getText().toString().contains("/")){
                    dialog = new DatePickerDialog(
                            InformationsTab.this.getContext(),
                            android.R.style.Theme_Holo_Dialog_MinWidth,
                            mOnDateSetListener,
                            year,month,day);
                }
                else {
                    LocalDate date = LocalDate.parse(mDisplayData.getText(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    dialog = new DatePickerDialog(
                            InformationsTab.this.getContext(),
                            android.R.style.Theme_Holo_Dialog_MinWidth,
                            mOnDateSetListener,
                            date.getYear(),date.getMonthValue()-1,date.getDayOfMonth());
                }
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                LocalDate date = LocalDate.of(year,month,dayOfMonth);
                mDisplayData.setText(date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            }
        };

        sendForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkIntegrity()) {

                    //POST request
                    StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    Log.d("Response", response);

                                    if (response != null) {
                                        try {
                                            response = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String idValues = response.split(",")[2];
                                    String userIdValue = idValues.split(":")[1];
                                    Log.d("user ID", userIdValue);

                                    PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).edit()
                                            .putString("userID", userIdValue).apply();

                                    storedId = PreferenceManager.
                                            getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("userID", "");

                                    Log.d("Stored user ID", storedId);

                                    String userDate = null;
                                    String userFirstName = null;
                                    String userLastName = null;
                                    String userLocation = null;
                                    String userMail = null;
                                    String userTelNum = null;

                                    String[] allInfos = response.split(",");
                                    userDate = allInfos[0].split(":")[1].replace("\"", "");
                                    userFirstName = allInfos[1].split(":")[1].replace("\"", "");
                                    userLastName = allInfos[3].split(":")[1].replace("\"", "");
                                    userLocation = allInfos[4].split(":")[1].replace("\"", "");
                                    userMail = allInfos[5].split(":")[1].replace("\"", "");
                                    userTelNum = allInfos[7].split(":")[1].replace("\"", "").replace("}", "").replace("]", "");

                                    try {
                                        user_profile = new JSONObject();

                                        setProfileInformations(user_profile, userFirstName, userLastName, userDate, userLocation, userMail, userTelNum);

                                        PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).edit()
                                                .putString("user_profile", user_profile.toString()).apply();

                                        getProfileInformations(user_profile);

                                        CharSequence text = "Votre profil a bien été créé";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast toast = Toast.makeText(v.getContext(), text, duration);
                                        toast.show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("firstName", firstname.getText().toString());
                            params.put("lastName", lastname.getText().toString());
                            params.put("date", mDisplayData.getText().toString());
                            params.put("mail", mail.getText().toString());
                            params.put("telNum", telNum.getText().toString());
                            params.put("location", localisations.getSelectedItem().toString());

                            return params;
                        }
                    };

                    sendForm.setVisibility(View.INVISIBLE);
                    modiForm.setVisibility(View.VISIBLE);

                    setWidgetOff();

                    queue.add(postRequest);

                } else {
                    CharSequence text = "Un ou plusieurs champs sont vides et ou sont invalides, vérifier puis valider à nouveau";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(v.getContext(), text, duration);
                    toast.show();

                    setWidgetsOn();
                }
            }
        });

        modiForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modiForm.setVisibility(View.INVISIBLE);
                validModif.setVisibility(View.VISIBLE);

                setWidgetsOn();
            }
        });

        validModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_url = url + "/" + storedId;
                Log.d("User url", user_url);

                if (checkIntegrity()) {

                    //PUT request
                    StringRequest putRequest = new StringRequest(Request.Method.PUT, user_url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    Log.d("Response", response.toString());

                                    if (response != null) {
                                        try {
                                            response = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String userDate = null;
                                    String userFirstName = null;
                                    String userLastName = null;
                                    String userLocation = null;
                                    String userMail = null;
                                    String userTelNum = null;

                                    String[] allInfos = response.split(",");
                                    userDate = allInfos[0].split(":")[1].replace("\"", "");
                                    userFirstName = allInfos[1].split(":")[1].replace("\"", "");
                                    userLastName = allInfos[3].split(":")[1].replace("\"", "");
                                    userLocation = allInfos[4].split(":")[1].replace("\"", "");
                                    userMail = allInfos[5].split(":")[1].replace("\"", "");
                                    userTelNum = allInfos[7].split(":")[1].replace("\"", "").replace("}", "").replace("]", "");

                                    Log.d("firstname", userFirstName);

                                    try {
                                        user_profile = new JSONObject();

                                        setProfileInformations(user_profile, userFirstName, userLastName, userDate, userLocation, userMail, userTelNum);

                                        PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).edit()
                                                .putString("user_profile", user_profile.toString()).apply();

                                        getProfileInformations(user_profile);

                                        CharSequence text = "Vos modifications ont bien été enregistrées";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast toast = Toast.makeText(v.getContext(), text, duration);
                                        toast.show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", error.toString());
                                }
                            }
                    ) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("firstName", firstname.getText().toString());
                            params.put("lastName", lastname.getText().toString());
                            params.put("date", mDisplayData.getText().toString());
                            params.put("mail", mail.getText().toString());
                            params.put("telNum", telNum.getText().toString());
                            params.put("location", localisations.getSelectedItem().toString());

                            return params;
                        }

                    };

                    validModif.setVisibility(View.INVISIBLE);
                    modiForm.setVisibility(View.VISIBLE);

                    setWidgetOff();

                    queue.add(putRequest);

                }else{
                    CharSequence text = "Un ou plusieurs champs sont vides et ou sont invalides, vérifier puis valider à nouveau";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(v.getContext(), text, duration);
                    toast.show();

                    setWidgetsOn();
                }
            }
        });

        return view;
    }

    public static boolean isEmailValid(String mail) {
        return mail.matches("[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
    }

    public boolean isNumValid(String telNume) {
        return telNume.length()==10;
    }

    public boolean checkIntegrity() {
        String fn = firstname.getText().toString();
        String ln = lastname.getText().toString();
        String date = mDisplayData.getText().toString();
        String m = mail.getText().toString();
        String tn = telNum.getText().toString();
        String l = localisations.getSelectedItem().toString();

        if(fn.isEmpty() || ln.isEmpty() || date.isEmpty() || l.isEmpty() || tn.isEmpty() || m.isEmpty() || (isNumValid(tn)==false) || (isEmailValid(m)==false)) {
            return false;
        }
        return true;
    }

    public void setProfileInformations(JSONObject profile, String userFirstName, String userLastName, String userDate, String userLocation, String userMail, String userTelNum) throws JSONException {
        profile.put("user_firstname", userFirstName);
        profile.put("user_lastname", userLastName);
        profile.put("user_date", userDate);
        profile.put("user_location", userLocation);
        profile.put("user_mail", userMail);
        profile.put("user_telnum", userTelNum);
    }

    public void getProfileInformations(JSONObject profile) throws JSONException {
        firstname.setText(profile.getString("user_firstname"));
        lastname.setText(profile.getString("user_lastname"));
        mail.setText(profile.getString("user_mail"));
        telNum.setText(profile.getString("user_telnum"));
        mDisplayData.setText(profile.getString("user_date"));
        Log.d("Check value profile", profile.getString("user_location"));
        location_display.setText(profile.getString("user_location"));
    }

    public void setWidgetsOn() {
        localisations.setVisibility(View.VISIBLE);
        location_display.setVisibility(View.INVISIBLE);
        firstname.setFocusableInTouchMode(true);
        lastname.setFocusableInTouchMode(true);
        telNum.setFocusableInTouchMode(true);
        mail.setFocusableInTouchMode(true);
        buttonBirthday.setEnabled(true);
        localisations.setEnabled(true);
    }

    public void setWidgetOff() {
        localisations.setVisibility(View.INVISIBLE);
        location_display.setVisibility(View.VISIBLE);
        firstname.setFocusable(false);
        lastname.setFocusable(false);
        mail.setFocusable(false);
        telNum.setFocusable(false);
        mDisplayData.setFocusable(false);
        buttonBirthday.setEnabled(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll("VOLLEY");
        }
    }

    private int getIndex(Spinner spinner, String myString){
        int index = 0;
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }

        return index;
    }
}
