package fr.ulille.iut.agile.client_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class WelcomeSlider extends AppCompatActivity {

    private ViewPager viewPager;
    private LinearLayout layoutDot;
    private TextView[] dots;
    private int[] layouts;
    private Button skipButton;
    private Button nextButton;
    private MyPagerAdapter pagerAdaptater;
    private CheckBox dontDisplayAgain;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isFirstTimeStartApp()) {
            startMainActivity();
            finish();
        }

        setStatusBarTransparent();
        setContentView(R.layout.welcome_slider);

        viewPager = (ViewPager) findViewById(R.id.view_slider);
        layoutDot = findViewById(R.id.dotLayout);
        nextButton = findViewById(R.id.btn_next);
        skipButton = findViewById(R.id.btn_skip);
        dontDisplayAgain = findViewById(R.id.dontDisplayAgain);
        dontDisplayAgain.setChecked(false);

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int currentPage = viewPager.getCurrentItem()+1;
                if(currentPage < layouts.length) {
                    viewPager.setCurrentItem(currentPage);
                }else{
                    startMainActivity();
                }
            }
        });

        dontDisplayAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(dontDisplayAgain.isChecked()) {
                    setFirstTimeStartStatus(false);
                }else {
                    setFirstTimeStartStatus(true);
                }
            }
        });

        layouts = new int[]{R.layout.slider1, R.layout.slider2, R.layout.slider3, R.layout.slider4};
        pagerAdaptater = new MyPagerAdapter(layouts, getApplicationContext());

        viewPager.setAdapter(pagerAdaptater);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == layouts.length-1) {
                    nextButton.setText("A TABLE !");
                    skipButton.setVisibility(View.GONE);

                }else {
                    nextButton.setText("SUIVANT");
                    skipButton.setVisibility(View.VISIBLE);

                }
                setDotsStatus(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setDotsStatus(0);

    }

    private boolean isFirstTimeStartApp() {
        SharedPreferences ref  = getApplicationContext().getSharedPreferences("IntroSliderApp", Context.MODE_PRIVATE);
        return ref.getBoolean("FirstTimeStartFlag", true);
    }

    private void setFirstTimeStartStatus(boolean stt) {
        SharedPreferences ref  = getApplicationContext().getSharedPreferences("IntroSliderApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = ref.edit();
        editor.putBoolean("FirstTimeStartFlag", stt);
        editor.commit();
    }

    private void setDotsStatus(int page){
        layoutDot.removeAllViews();
        dots = new TextView[layouts.length];
        for(int i =0; i<dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(30);
            dots[i].setTextColor(Color.parseColor("#a9b4bb"));
            layoutDot.addView(dots[i]);
        }

        if(dots.length>0) {
            dots[page].setTextColor(Color.parseColor("#ffffff"));

        }
    }

    private void startMainActivity() {
        Intent i = new Intent(WelcomeSlider.this, MainActivity.class);
        boolean flagFirstLaunch = true;
        i.putExtra("Flag", flagFirstLaunch);
        startActivity(i);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarTransparent() {
        if(Build.VERSION.SDK_INT >=2){
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE|View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
