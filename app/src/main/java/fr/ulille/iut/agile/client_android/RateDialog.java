package fr.ulille.iut.agile.client_android;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;

public class RateDialog extends Dialog {


    public RateDialog(@NonNull Context context) {
        super(context);
        this.setCancelable(false);
    }

    public RateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        this.dismiss();
    }
}
