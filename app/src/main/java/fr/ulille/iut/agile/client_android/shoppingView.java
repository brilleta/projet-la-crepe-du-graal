package fr.ulille.iut.agile.client_android;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class shoppingView extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_screen);

        WebView shoppingview = new WebView(this);
        setContentView(shoppingview);
        shoppingview.loadUrl("https://www.connectill.com/");

    }
}
