package fr.ulille.iut.agile.client_android;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LocalisationTab extends Fragment {

    private MapView mMapView;
    private GoogleMap map;
    private FloatingActionButton calendarButton;
    private FloatingActionButton mapButton;
    private final String url = "https://groupe8.azae.eu/api/v1/locations";
    private RequestQueue queue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.localisation_tab, container, false);
        setRetainInstance(true);
        Date currentTime = Calendar.getInstance().getTime();
        String[] dateInfos = currentTime.toString().split(" ");
        Log.w("TodayDate", dateInfos[0] + " " + dateInfos[3]);

        calendarButton = view.findViewById(R.id.calendarButton);
        mapButton = view.findViewById(R.id.mapButton);
        mapButton.setVisibility(View.INVISIBLE);
        mMapView = (MapView) view.findViewById(R.id.map);
        queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                }

               // List<List<String>> locations = doGet(getView());
                List<String> places = new ArrayList<>();
                List<String> coord = new ArrayList<>();
                List<String> dates = new ArrayList<>();
                List<String> infos = new ArrayList<>();
                List<String> hours = new ArrayList<>();


                JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                int taille = response.length();
                                for (int i = 0; i < taille; i++) {
                                    try {
                                        JSONObject location = response.getJSONObject(i);
                                        String place = location.getString("places");
                                        String coord = location.getString("coord");
                                        String date = location.getString("date");
                                        String infos = location.getString("infos");
                                        String hours = location.getString("hours");

                                        float lat = Float.parseFloat(coord.split(",")[0]);
                                        float lng = Float.parseFloat(coord.split(",")[1]);
                                        LatLng latLng = new LatLng(lat,lng);
                                        MarkerOptions m = new MarkerOptions().position(latLng).title(place);
                                        String snippet;
                                        String currentDay = dateInfos[0];
                                        String dayTruck = date;
                                        LocalTime currentTime = parseLocalTime(dateInfos[3]);
                                        LocalTime foodtruckOpenTime = parseLocalTime(hours.split(" ")[0]);
                                        LocalTime foodtruckCloseTime = parseLocalTime(hours.split(" ")[1]);
                                        if(currentDay.equals(dayTruck) && currentTime.isAfter(foodtruckOpenTime) && currentTime.isBefore(foodtruckCloseTime)){
                                            snippet = "Ouvert\n"+infos;
                                            m.icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_on));
                                        }
                                        else {
                                            snippet = "Fermé aujourd'hui\n"+infos;
                                            m.icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_off));
                                        }
                                        m.snippet(snippet);
                                        map.addMarker(m);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        },
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error.Response", error.toString());
                            }
                        }
                );
                Log.d("FIN REQUEST",places.size()+": SIZE WTF");
                queue.add(getRequest);


                Log.d("LOCATION",coord.size()+"");
                for(int i = 0; i<coord.size(); i++){
                    float lat = Float.parseFloat(coord.get(i).split(",")[0]);
                    float lng = Float.parseFloat(coord.get(i).split(",")[1]);
                    LatLng latLng = new LatLng(lat,lng);
                    MarkerOptions m = new MarkerOptions().position(latLng).title(places.get(i));
                    String snippet;
                    String currentDay = dateInfos[0];
                    String dayTruck = dates.get(i);
                    LocalTime currentTime = parseLocalTime(dateInfos[3]);
                    LocalTime foodtruckOpenTime = parseLocalTime(hours.get(i).split(" ")[0]);
                    LocalTime foodtruckCloseTime = parseLocalTime(hours.get(i).split(" ")[1]);
                    if(currentDay.equals(dayTruck) && currentTime.isAfter(foodtruckOpenTime) && currentTime.isBefore(foodtruckCloseTime)){
                        snippet = "Ouvert\n"+infos.get(i);
                        m.icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_on));
                    }
                    else {
                        snippet = "Fermé aujourd'hui\n"+infos.get(i);
                        m.icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_off));
                    }
                    m.snippet(snippet);
                    map.addMarker(m);
                }
                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        LinearLayout info = new LinearLayout(getContext());
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(getContext());
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(getContext());
                        snippet.setTextColor(Color.GRAY);
                        snippet.setGravity(Gravity.CENTER);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }
                });
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(50.619692, 3.131264)).zoom(11).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        calendarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                PlanningTab planning = new PlanningTab();
                (getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.mapFragment, planning, "findThisFragment")
                        .addToBackStack(null)
                        .commit();

                calendarButton.setVisibility(View.INVISIBLE);
                mapButton.setVisibility(View.VISIBLE);
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LocalisationTab localisation = new LocalisationTab();
                (getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.planning_fragment, localisation, "findThisFragment")
                        .addToBackStack(null)
                        .commit();

                mapButton.setVisibility(View.INVISIBLE);
                calendarButton.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private LocalTime parseLocalTime(String dateInfo) {
        String [] currentTime = dateInfo.split(":");
        return LocalTime.of(Integer.parseInt(currentTime[0]),Integer.parseInt(currentTime[1]),Integer.parseInt(currentTime[2]));
    }


    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll("VOLLEY");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}