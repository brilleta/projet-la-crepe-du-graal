package fr.ulille.iut.agile.client_android;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ContactTab extends Fragment {

    View view;
    Button contactButton;
    WebView devisPriv;

    public ContactTab() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_tab, container, false);
        setRetainInstance(true);
        contactButton = view.findViewById(R.id.contactButton);

        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                String aEmailList[] = { "contact-crepedugraal@mail.com"};
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Demande de renseignements");
                emailIntent.setType("plain/text");

                startActivity(Intent.createChooser(emailIntent, "Chosissez votre messagerie :"));
            }
        });

        devisPriv = view.findViewById(R.id.devisPrivatisation);
        devisPriv.loadUrl("https://docs.google.com/forms/d/e/1FAIpQLSf-PfdlaYMz398C3wURxPyOrtkJi0W8DJZCRc8HGa203A7wbQ/viewform");

        return view;
    }
}