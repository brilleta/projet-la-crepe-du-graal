package fr.ulille.iut.agile.client_android;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import interfaces.DialogCallback;
import utils.GlobalUtils;

public class CommandTab extends Fragment {

    ImageSwitcher IS;
    Button menuButton;
    Button crepesButton;
    View view;
    FloatingActionButton fab;
    View menuLayout;
    FloatingActionButton rateUs;

    public CommandTab(){}

    @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.commands_tab, container, false);
        setRetainInstance(true);
        IS = (ImageSwitcher) view.findViewById(R.id.imageSwitcher);
        IS.setFactory(() -> {
            ImageView IV = new ImageView(getActivity().getApplicationContext());
            IV.setScaleType(ImageView.ScaleType.FIT_CENTER);
            IV.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
            return IV;
        });

        menuLayout = view.findViewById(R.id.command_menu);
        menuButton = (Button)menuLayout.findViewById(R.id.menuButton);
        crepesButton = (Button)menuLayout.findViewById(R.id.crepesButton);
        IS.setImageResource(R.drawable.commandes_carte_2);

        menuButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                IS.setImageResource(R.drawable.commandes_carte_2);
            }
        });

        crepesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                IS.setImageResource(R.drawable.commandes_carte_1);
            }
        });

        fab = view.findViewById(R.id.goShop);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Test commande", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                Intent shopIntent = new Intent(view.getContext(), shoppingView.class);
                startActivity(shopIntent);
            }
        });

        rateUs = view.findViewById(R.id.rateUs);
        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(v);
            }
        });

        return view;
    }

    public void showDialog(View view) {
        GlobalUtils.showDialog(this.getContext(), new DialogCallback() {
            @Override
            public void callBack(String ratings) {

            }
        });
    }


}

