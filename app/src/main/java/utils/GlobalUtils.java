package utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hsalf.smilerating.SmileRating;

import java.util.HashMap;
import java.util.Map;

import fr.ulille.iut.agile.client_android.R;
import fr.ulille.iut.agile.client_android.RateDialog;
import interfaces.DialogCallback;

public class GlobalUtils {

    public static String rating = "0";
    private static RequestQueue queue;
    private static String storedId;
    private static String ratingValue;

    public static void showDialog(Context context, DialogCallback dialogCallback) {

        final RateDialog dialog = new RateDialog(context, R.style.RateDialogTheme);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.rate_dialog, null);

        dialog.setContentView(v);
        TextView done_button = (TextView) dialog.findViewById(R.id.rateUsDone);
        TextView cancel_button = (TextView) dialog.findViewById(R.id.cancelRate);
        SmileRating smileRating = (SmileRating) dialog.findViewById(R.id.smile_rating);

        queue = Volley.newRequestQueue(context.getApplicationContext());
        storedId = PreferenceManager.
                getDefaultSharedPreferences(context.getApplicationContext()).getString("userID","");
        Log.d("Stored user ID", storedId);
        String rating_url = "https://groupe8.azae.eu/api/v1/customers/" + storedId + "/rating";

        smileRating.setNameForSmile(SmileRating.GREAT, "Excellent");
        smileRating.setNameForSmile(SmileRating.GOOD, "Très bon");
        smileRating.setNameForSmile(SmileRating.OKAY, "Bon");
        smileRating.setNameForSmile(SmileRating.BAD, "Moyen");
        smileRating.setNameForSmile(SmileRating.TERRIBLE, "Mauvais");

        smileRating.setOnSmileySelectionListener(new SmileRating.OnSmileySelectionListener() {
            @Override
            public void onSmileySelected(int smiley, boolean reselected) {
                switch (smiley){
                    case SmileRating.BAD:
                        Log.i("Rating:", "Bad");
                        rating = "2";
                        break;
                    case SmileRating.GOOD:
                        Log.i("Rating:", "Good");
                        rating = "4";
                        break;
                    case SmileRating.GREAT:
                        Log.i("Rating:", "Great");
                        rating = "5";
                        break;
                    case SmileRating.OKAY:
                        Log.i("Rating:", "Okay");
                        rating = "3";
                        break;
                    case SmileRating.TERRIBLE:
                        Log.i("Rating:", "Terrible");
                        rating = "1";
                        break;
                }
            }
        });

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogCallback != null)
                    dialogCallback.callBack(rating);
                dialog.dismiss();

                //PUT request
                StringRequest postRequest = new StringRequest(Request.Method.PUT, rating_url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                                Log.d("Stored user ID", storedId);

                                String userRating = response;
                                PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).edit()
                                        .putString("ratingValue", userRating).apply();

                                ratingValue = PreferenceManager.
                                        getDefaultSharedPreferences(context.getApplicationContext()).getString("ratingValue","");

                                CharSequence text = "Votre note : " + rating + " | Merci pour votre retour !";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(v.getContext(), text, duration);
                                toast.show();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", error.toString());

                                CharSequence text = "Pour noter nos services veuillez vous enregistrer";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(v.getContext(), text, duration);
                                toast.show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("rating", rating);

                        return params;
                    }
                };

                queue.add(postRequest);
            }
        });
        dialog.show();
    }
}
